package xyz.flirora.thornmod;

import net.fabricmc.api.ModInitializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThornMod implements ModInitializer {
	public static final Logger LOGGER = LoggerFactory.getLogger("þ");

	@Override
	public void onInitialize() {
		LOGGER.info("þ");
	}
}