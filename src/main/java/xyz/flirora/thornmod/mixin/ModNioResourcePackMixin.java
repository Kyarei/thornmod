package xyz.flirora.thornmod.mixin;

import net.fabricmc.fabric.impl.resource.loader.ModNioResourcePack;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

@Mixin(value = ModNioResourcePack.class, remap = false)
public class ModNioResourcePackMixin {
    @ModifyConstant(method = "<clinit>", constant = @Constant(stringValue = "[a-z0-9-_.]+"))
    private static String modifyRegex(String regex) {
        return "[a-z0-9-_.þ]+";
    }
}
