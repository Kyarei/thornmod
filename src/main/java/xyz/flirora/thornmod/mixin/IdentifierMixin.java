package xyz.flirora.thornmod.mixin;

import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Identifier.class)
public class IdentifierMixin {
	@Inject(at = @At("HEAD"), method = "isNamespaceCharacterValid", cancellable = true)
	private static void onIsNamespaceCharacterValid(char character, CallbackInfoReturnable<Boolean> cir) {
		if (character == 'þ') {
			cir.setReturnValue(true);
		}
	}

	@Inject(at = @At("HEAD"), method = "isPathCharacterValid", cancellable = true)
	private static void onIsPathCharacterValid(char character, CallbackInfoReturnable<Boolean> cir) {
		if (character == 'þ') {
			cir.setReturnValue(true);
		}
	}

	@Inject(at = @At("HEAD"), method = "isCharValid", cancellable = true)
	private static void onIsCharValid(char character, CallbackInfoReturnable<Boolean> cir) {
		if (character == 'þ') {
			cir.setReturnValue(true);
		}
	}

	@ModifyArg(method = "validateNamespace", at = @At(value = "INVOKE", target = "Lnet/minecraft/util/InvalidIdentifierException;<init>(Ljava/lang/String;)V"), index = 0)
	private static String modifyValidateNamespaceExceptionMessage(String message) {
		return message.replace("]", "þ]");
	}

	@ModifyArg(method = "validatePath", at = @At(value = "INVOKE", target = "Lnet/minecraft/util/InvalidIdentifierException;<init>(Ljava/lang/String;)V"), index = 0)
	private static String modifyValidatePathExceptionMessage(String message) {
		return message.replace("]", "þ]");
	}
}