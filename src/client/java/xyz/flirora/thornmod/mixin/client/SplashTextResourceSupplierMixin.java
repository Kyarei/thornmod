package xyz.flirora.thornmod.mixin.client;

import net.minecraft.client.gui.screen.SplashTextRenderer;
import net.minecraft.client.resource.SplashTextResourceSupplier;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(SplashTextResourceSupplier.class)
public class SplashTextResourceSupplierMixin {
	@Inject(method = "get", at = @At("RETURN"), cancellable = true)
	private void modifySplashText(CallbackInfoReturnable<SplashTextRenderer> cir) {
		SplashTextRenderer r = cir.getReturnValue();
		if (r == null) return;
		String s = ((SplashTextRendererAccessor) r).getText();
		if (s.endsWith(" IS YOU")) {
			cir.setReturnValue(new SplashTextRenderer(s.substring(0, s.length() - 7) + " ÞÞ ÞÞÞ"));
		} else {
			StringBuffer buf = new StringBuffer();
			s.codePoints().forEach(c -> {
				if (Character.isAlphabetic(c)) {
					buf.append(Character.isUpperCase(c) ? 'Þ' : 'þ');
				} else {
					buf.appendCodePoint(c);
				}
			});
			cir.setReturnValue(new SplashTextRenderer(buf.toString()));
		}
	}
}