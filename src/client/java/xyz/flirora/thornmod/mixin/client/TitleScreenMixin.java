package xyz.flirora.thornmod.mixin.client;

import net.minecraft.client.gui.screen.TitleScreen;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

@Mixin(TitleScreen.class)
public class TitleScreenMixin {
    @ModifyConstant(method = "<clinit>", constant = @Constant(stringValue = "Copyright Mojang AB. Do not distribute!"))
    private static String modifyCopyrightMessage(String old) {
        return "Coþyright Mojang AB. Do not distribute!";
    }
}
